import json
import os
from random import choice, randint
from typing import Any

from tor_weather.utilities import read_json


class MockData:

    subscriptions: list[str] = [
        "node_down_sub",
        "node_bandwidth_sub",
        "node_version_sub",
        "node_flag_fast_sub",
        "node_flag_exit_sub",
        "node_flag_guard_sub",
        "node_flag_stable_sub",
        "node_flag_valid_sub",
    ]

    def _get_subscriptions(self) -> list[Any]:
        """Read all subscriptions from the mock data"""
        mock_file_path = "../mock/database/subscriptions.json"
        mock_data = read_json(os.path.join(os.path.dirname(__file__), mock_file_path))
        subscriptions = mock_data.get("payload")
        return subscriptions

    def _get_child_sub_object(self, subscription: str, id: int, subscription_id: int):
        """Get object for the child subscription"""
        if subscription != "node_bandwidth_sub":
            return {
                "id": id,
                "subscription_id": subscription_id,
                "is_active": choice([True, False]),
                "wait_for": randint(0, 100),
            }
        else:
            return {
                "id": id,
                "subscription_id": subscription_id,
                "is_active": choice([True, False]),
                "wait_for": randint(0, 100),
                "threshold": randint(0, 5000),
            }

    def _write_subs_to_json(self, result):
        """Write the results to JSON"""
        for subscription in result.keys():
            file_path = f"../mock/database/{subscription}.json"
            with open(os.path.join(os.path.dirname(__file__), file_path), "w") as i:
                json.dump({"payload": result[subscription]}, i)
        print("Written Subs to the JSON")

    def create_subscriptions(self):
        """Create child subscriptions for the subscriptions"""
        result = {}
        subscriptions: list[Any] = self._get_subscriptions()
        for i in range(0, len(subscriptions)):
            subscription_name = self.subscriptions[(i // 100)]
            child_sub = self._get_child_sub_object(
                subscription=subscription_name, id=(i % 100) + 1, subscription_id=i + 1
            )
            if not result.get(subscription_name):
                result[subscription_name] = []
            result[subscription_name].append(child_sub)
        self._write_subs_to_json(result=result)
