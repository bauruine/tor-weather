#!/bin/bash

# Basic Steps
sudo apt update
sudo apt install nano

# Install Git
sudo apt install git -y
sudo apt install nginx -y

# Configure Firewall
sudo iptables -I INPUT 6 -m state --state NEW -p tcp --dport 80 -j ACCEPT
sudo netfilter-persistent save

# Install Postgres
sudo apt install postgresql postgresql-contrib -y

# Install Python3.9
sudo apt install software-properties-common -y
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt update
sudo apt install python3.9 -y
sudo apt install python3.9-distutils -y
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.9 get-pip.py
export PATH=/home/ubuntu/.local/bin:$PATH

# Install Pipenv
pip3.9 install pipenv

# Setup Database
sudo -u postgres createuser ubuntu --superuser
sudo -u ubuntu createdb torweatherdev
sudo -u ubuntu createdb torweatherprod
sudo -u ubuntu psql -c "ALTER USER ubuntu WITH PASSWORD 'test123';"

# Setup Repository
git clone https://gitlab.torproject.org/tpo/network-health/tor-weather.git
cd tor-weather
pipenv sync
touch .env

# To be done manually
# 1. Add nginx config
# 2. Add environment variables
