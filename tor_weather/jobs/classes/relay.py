from datetime import datetime
from typing import Optional, cast

from sqlalchemy import exc

from tor_weather.core.database import Relay
from tor_weather.extensions import db, job_logger
from tor_weather.models.details_payload import (
    BandwidthInterface,
    DateInterface,
    FlagInterface,
    RelayPayloadInterface,
    VersionInterface,
)


class RelayObject:
    """Object Representing a Relay"""

    def __init__(self, relay: RelayPayloadInterface) -> None:
        self.fingerprint = relay["fingerprint"]
        self.version = self._get_version_details(relay)
        self.date = self._get_first_and_last_seen_details(relay)
        self.flag = self._get_flag_details(relay)
        self.bandwidth = self._get_bandwidth_details(relay)
        self.running = relay["running"]

    def __repr__(self) -> str:
        return f"<RelayClass: fingerprint: {self.fingerprint}, running: {self.running}>"

    def _get_database_instance(self) -> Optional[Relay]:
        """
        Get instance of relay from the database
        """
        db_relay: Optional[Relay] = Relay.query.filter_by(
            fingerprint=self.fingerprint
        ).first()
        return db_relay

    def _parse_date(self, date: str) -> datetime:
        """Parses the date into datetime object.

        Args:
            date (str): Datetime in string format

        Returns:
            datetime: Datetime in datetime format
        """
        return datetime.strptime(date, "%Y-%m-%d %H:%M:%S")

    def _change_bandwidth_units(self, bandwidth: Optional[int]) -> Optional[int]:
        bandwidth = bandwidth // 1000 if bandwidth else None
        return bandwidth

    def _get_version_details(self, relay: RelayPayloadInterface) -> VersionInterface:
        """Parses the version information from the relay data

        Args:
            relay (RelayPayloadInterface): API relay object

        Returns:
            VersionInterface: Parsed relay object
        """
        return {
            "id": relay.get("version"),
            "status": relay.get("version_status"),
            "recommended": relay.get("recommended_version"),
            "platform": relay.get("platform"),
        }

    def _get_bandwidth_details(
        self, relay: RelayPayloadInterface
    ) -> BandwidthInterface:
        """Parses the bandwidth information from the relay data

        Args:
            relay (RelayPayloadInterface): API Relay object

        Returns:
            BandwidthInterface: Parsed bandwidth object
        """
        return {
            "observed": self._change_bandwidth_units(relay.get("observed_bandwidth")),
            "burst": self._change_bandwidth_units(relay.get("bandwidth_burst")),
            "rate": self._change_bandwidth_units(relay.get("bandwidth_rate")),
            "advertised": self._change_bandwidth_units(
                relay.get("advertised_bandwidth")
            ),
        }

    def _get_first_and_last_seen_details(
        self, relay: RelayPayloadInterface
    ) -> DateInterface:
        """Parses the first & last seen information from the relay data

        Args:
            relay (RelayPayloadInterface): API relay object

        Returns:
            DateInterface: Parsed first & last seen object
        """
        return {
            "first_seen": self._parse_date(relay["first_seen"]),
            "last_seen": self._parse_date(relay["last_seen"]),
        }

    def _get_flag_details(self, relay: RelayPayloadInterface) -> FlagInterface:
        """Parses the flags information from the relay data

        Args:
            relay (RelayPayloadInterface): API relay object

        Returns:
            FlagInterface: Parsed flags object
        """
        flags = relay["flags"]
        return {
            "exit": "Exit" in flags if flags else False,
            "guard": "Guard" in flags if flags else False,
            "running": "Running" in flags if flags else False,
            "stable": "Stable" in flags if flags else False,
            "fast": "Fast" in flags if flags else False,
            "valid": "Valid" in flags if flags else False,
        }

    def _add_relay_to_db(self) -> None:
        """
        Add a relay to the database
        """
        relay: Relay = Relay(
            fingerprint=self.fingerprint,
            is_exit=self.flag["exit"],
            first_seen=self.date["first_seen"],
            last_seen=self.date["last_seen"],
            version=self.version["id"],
            recommended_version=self.version["recommended"],
            is_up=self.running,
        )
        db.session.add(relay)
        try:
            db.session.commit()
        except exc.SQLAlchemyError as e:
            job_logger.error(f"Adding relay to db failed - {e}")
            db.session.rollback()

    def get_database_obj(self) -> Relay:
        """
        Get the database object for the relay
        """
        # 1. Get the instance of relay from the database
        db_relay = self._get_database_instance()
        if db_relay:
            # If relay exists in db, return it
            return db_relay
        else:
            # If relay doesn't exit, add it in database
            self._add_relay_to_db()
            # And return it's database instance
            return cast(Relay, self._get_database_instance())
