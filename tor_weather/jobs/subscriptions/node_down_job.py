from datetime import datetime
from typing import Any, Type

from tor_weather.core.mail import NodeDownEmail
from tor_weather.extensions import job_logger
from tor_weather.jobs.classes.relay import RelayObject
from tor_weather.jobs.classes.subscription import SubscriptionObject
from tor_weather.jobs.subscriptions.subscription_job import SubscriptionJob
from tor_weather.utilities import time_diff


class NodeDownSubscriptionJob(SubscriptionJob):
    def __init__(self, subscription_obj: SubscriptionObject, relay_obj: RelayObject):
        super().__init__(subscription_obj, relay_obj)
        self.email_class: Type[NodeDownEmail] = NodeDownEmail  # type: ignore
        self.child_sub_name: str = "node_down_sub"
        self.set_child_subscription(self.child_sub_name)

    def get_data_for_mail(self) -> Any:
        return {
            "fingerprint": self.relay.fingerprint,
            "wait_for": self.subscription.get_value("wait_for"),
        }

    def validate(self) -> None:
        if self.subscription.child_sub and self.subscription.get_value("is_active"):
            if self.subscription.get_value("running"):
                # Relay was previously running
                if not self.relay.running:
                    # Relay was running & just went down
                    self.subscription.set_value("issue_first_seen", datetime.utcnow())
                    self.subscription.set_value("running", False)
            else:
                # Relay was previously down
                if self.relay.running:
                    # Relay was down & just started running
                    self.subscription.set_value("issue_first_seen", None)
                    self.subscription.set_value("running", True)
                    self.subscription.set_value("emailed", False)
                else:
                    # Relay was down & is still down
                    issue_first_seen = self.subscription.get_value("issue_first_seen")
                    wait_for = self.subscription.get_value("wait_for")
                    if time_diff(issue_first_seen, datetime.utcnow()) >= wait_for:
                        # Relay has been down for more than the waiting time
                        if not self.subscription.get_value("emailed"):
                            # Subscriber was not sent an email already
                            mail_data = self.get_data_for_mail()
                            try:
                                self.send_email(mail_data)
                                self.subscription.set_value("emailed", True)
                            except Exception as e:
                                job_logger.exception(f"Failed to send an email - {e}")
            self.subscription.commit()
