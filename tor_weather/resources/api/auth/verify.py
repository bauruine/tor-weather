from flask import request
from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.error import BadRequest
from tor_weather.routes import api_ns
from tor_weather.service.user import User

verify_parser = api_ns().parser()
verify_parser.add_argument("code", type=str, location="args", required=True)


class VerifyApi(Resource):
    """Implements API for Verification"""

    @api_ns().response(302, "Redirect to Login or Incorrect Credentials")
    def get(self) -> Response:
        verification_code: str = request.args["code"]
        if verification_code:
            return User().verify_email(verification_code)
        else:
            raise BadRequest("Required inputs not passed")
