from flask import make_response, render_template
from flask.wrappers import Response
from flask_restx import Resource

from tor_weather.extensions import backend_logger
from tor_weather.routes import template_ns
from tor_weather.service.header import Header as TemplateHeader


class RegisterTemplate(Resource):
    """Implements the Register Page"""

    @template_ns().response(200, "Success")
    def get(self) -> Response:
        backend_logger.info("Register Template Requested")
        # Get the value of template variables for the route
        header = TemplateHeader("register").get_options()
        # Create a response with the template variables
        return make_response(render_template("pages/register.html", header=header))
