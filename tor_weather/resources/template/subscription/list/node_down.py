from typing import cast

from flask import make_response, render_template, request, url_for
from flask.wrappers import Response
from flask_login import current_user, login_required
from flask_restx import Resource

from tor_weather.core.database.tables.subscriber import Subscriber
from tor_weather.extensions import backend_logger
from tor_weather.routes import template_ns
from tor_weather.service.breadcrumb import Breadcrumb
from tor_weather.service.sidebar import Sidebar
from tor_weather.service.subscription.node_down import NodeDownService


class NodeDownListTemplate(Resource):
    """Implements the Node-Down Subscription List Page"""

    @login_required
    @template_ns().response(200, "Success")
    def get(self) -> Response:
        backend_logger.info("Node Down List Template Requested")

        subscriber = cast(Subscriber, current_user)
        email = subscriber.email

        sidebar = Sidebar(request.path).get_data()
        breadcrumb = Breadcrumb(request.path).get_data()
        table_data = NodeDownService(email).get_subscriptions()
        create_url: str = url_for("node_down_create")

        return make_response(
            render_template(
                "/pages/dashboard/subscription-list.html",
                sidebar=sidebar,
                breadcrumb=breadcrumb,
                table=table_data,
                createUrl=create_url,
            )
        )
