from typing import Any

from tor_weather.constants import SubscriptionType
from tor_weather.core.database.tables import NodeBandwidthSub
from tor_weather.dao.subscription.subscription_dao import SubscriptionDao


class NodeBandwidthDao(SubscriptionDao):
    def __init__(self, email: str, flag=None):
        super().__init__(email)
        self.redirect_url = "/dashboard/node-status/node-bandwidth"
        self.subscription_type = SubscriptionType["node-bandwidth"]

    def _create_current_sub(self, data: Any) -> NodeBandwidthSub:
        """Create a current subscription object"""
        subscription = NodeBandwidthSub(
            wait_for=data["wait_for"], threshold=data["threshold"]
        )
        return subscription

    def _modify_current_sub(self, child_sub: NodeBandwidthSub, data: Any) -> None:
        """Modify the current subscription object"""
        child_sub.wait_for = data["wait_for"]
        child_sub.threshold = data["threshold"]
