import re
from typing import Any, List, Optional

from flask import flash, make_response, redirect

from tor_weather.constants import SnackbarMessage
from tor_weather.core.database.tables import Relay, Subscriber, Subscription
from tor_weather.error import BadRequest
from tor_weather.extensions import db
from tor_weather.models.constants_model import SubscriptionTypeInterface


class SubscriptionDao:
    def __init__(self, email: str, flag: Optional[str] = None):
        self.email: str = email
        self.flag: Optional[str] = flag
        self.subscription_type: SubscriptionTypeInterface
        self.subscriber: Subscriber
        self.redirect_url: str

    def _validate_fingerprint_format(self, fingerprint: str) -> bool:
        if re.fullmatch("^[A-Fa-f0-9]{40}$", fingerprint):
            return True
        return False

    def _get_translated_data(self, fingerprint: str, child_sub: Any):
        """Get translated payload for the subscription data"""
        return {"fingerprint": fingerprint, "child": child_sub}

    def _get_subscriber(self) -> Subscriber:
        """Get subscriber object from the email-id"""
        return Subscriber.query.filter_by(email=self.email).first()

    def _get_relay(self, fingerprint: str) -> Optional[Relay]:
        """Get relay object from the fingerprint"""
        return Relay.query.filter_by(fingerprint=fingerprint).first()

    def _get_global_subs_for_user(self) -> List[Subscription]:
        """Get list of global subscriptions for the current user"""
        self.subscriber = self._get_subscriber()
        return self.subscriber.subscriptions

    def _get_current_subs_for_user(self, subscriptions: List[Subscription]):
        """Get list of current subscriptions for the current user"""
        current_subs = []
        for global_sub in subscriptions:
            child_sub = getattr(global_sub, self.subscription_type.get("db_value"))  # type: ignore
            if child_sub:
                fingerprint: str = global_sub.relay.fingerprint
                translated_data = self._get_translated_data(fingerprint, child_sub)
                current_subs.append(translated_data)
        return current_subs

    def _get_global_sub_for_fingerprint(
        self, fingerprint: str
    ) -> Optional[Subscription]:
        """Get global subscription of the current user for the fingerprint"""
        global_subs: List[Subscription] = self._get_global_subs_for_user()
        for global_sub in global_subs:
            if global_sub.relay.fingerprint == fingerprint:
                return global_sub
        return None

    def _create_global_sub(self, fingerprint: str) -> Subscription:
        """Create a global subscription object"""
        subscriber: Subscriber = self._get_subscriber()
        relay = self._get_relay(fingerprint)
        return Subscription(subscriber_id=subscriber.id, relay_id=relay.id)

    def _create_current_sub(self, data: Any):
        """Create a current subscription object"""
        pass

    def _modify_current_sub(self, child_sub: Any, data: Any):
        """Modify the current subscription object"""
        pass

    def get_subscriptions(self):
        """Get list of all the subscriptions of the user"""
        global_subs: List[Subscription] = self._get_global_subs_for_user()  # type: ignore
        return self._get_current_subs_for_user(global_subs)

    def get_subscription_data(self, fingerprint: str):
        """Get data for a subscription"""
        global_sub = self._get_global_sub_for_fingerprint(fingerprint)
        # Check if the user already has a global subscription for the fingerprint.
        if global_sub:
            # If there is a global subscription
            child_sub = getattr(global_sub, self.subscription_type.get("db_value"))  # type: ignore
            # If there is the child subscription already
            if child_sub:
                return child_sub
            else:
                raise BadRequest("User does not have this subscription with the node")
        else:
            raise BadRequest("User does not have a subscription with the node")

    def create_subscription(self, data: Any):
        """Create a subscription for the user"""
        if self._validate_fingerprint_format(data.get("fingerprint")):
            global_sub = self._get_global_sub_for_fingerprint(data.get("fingerprint"))
            if global_sub:
                child_sub = getattr(global_sub, self.subscription_type.get("db_value"))  # type: ignore
                if child_sub:
                    flash(**SnackbarMessage.SUBSCRIPTION_EXISTS.value)
                    return make_response(redirect(self.redirect_url))
                else:
                    # Create a child subscription, and attach it to the global subscription.
                    child_sub = self._create_current_sub(data)
                    setattr(
                        global_sub, self.subscription_type.get("db_value"), child_sub
                    )
                    # type: ignore
                    db.session.add(global_sub)
                    db.session.commit()
                    flash(**SnackbarMessage.SUBSCRIPTION_CREATED.value)
                    return make_response(redirect(self.redirect_url))
            else:
                relay = self._get_relay(fingerprint=data.get("fingerprint"))
                if relay:
                    global_sub = self._create_global_sub(
                        fingerprint=data.get("fingerprint")
                    )
                    child_sub = self._create_current_sub(data)
                    setattr(global_sub, self.subscription_type.get("db_value"), child_sub)  # type: ignore
                    db.session.add(global_sub)
                    db.session.commit()
                    flash(**SnackbarMessage.SUBSCRIPTION_CREATED.value)
                    return make_response(redirect(self.redirect_url))
                else:
                    flash(**SnackbarMessage.RELAY_NOT_FOUND.value)
                    return make_response(redirect(self.redirect_url))
        else:
            flash(**SnackbarMessage.WRONG_FINGERPRINT.value)
            return make_response(redirect(self.redirect_url))

    def modify_subscription(self, data: Any):
        """Modify a subscription for the user"""
        # EXTRACT FINGERPRINT FROM THE DATA
        if self._validate_fingerprint_format(data.get("fingerprint")):
            global_sub = self._get_global_sub_for_fingerprint(data.get("fingerprint"))
            # Check if the user already has a global subscription for the fingerprint.
            if global_sub:
                # If there is a global subscription
                child_sub = getattr(global_sub, self.subscription_type.get("db_value"))  # type: ignore
                # If there is the child subscription, modify it, and commit the changes
                if child_sub:
                    self._modify_current_sub(child_sub, data)
                    db.session.add(child_sub)
                    db.session.commit()
                    flash(**SnackbarMessage.SUBSCRIPTION_MODIFIED.value)
                    return make_response(redirect(self.redirect_url))
                # If there isnt a child subscription, throw an error that sub does not exist.
                else:
                    # Throw an error that "Subscription already exists. Edit it instead!"
                    flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                    return make_response(redirect(self.redirect_url))
            # If there isn't a global subscription
            else:
                # Throw an error that "Subscription does not exist. Please create one instead."
                flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                return make_response(redirect(self.redirect_url))
        else:
            flash(**SnackbarMessage.WRONG_FINGERPRINT.value)
            return make_response(redirect(self.redirect_url))

    def deactivate_subscription(self, data):
        """Deactivate a subscription for the user"""
        # EXTRACT FINGERPRINT FROM THE DATA
        if self._validate_fingerprint_format(data.get("fingerprint")):
            global_sub = self._get_global_sub_for_fingerprint(data.get("fingerprint"))
            # Check if the user already has a global subscription for the fingerprint.
            if global_sub:
                # If there is a global subscription
                child_sub = getattr(global_sub, self.subscription_type.get("db_value"))  # type: ignore
                # If there is the child subscription, modify it, and commit the changes
                if child_sub:
                    child_sub.is_active = False
                    db.session.add(child_sub)
                    db.session.commit()
                    flash(**SnackbarMessage.SUBSCRIPTION_DISABLED.value)
                    return make_response(redirect(self.redirect_url))
                # If there isnt a child subscription, throw an error that sub does not exist.
                else:
                    # Throw an error that "Subscription already exists. Edit it instead!"
                    flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                    return make_response(redirect(self.redirect_url))
            # If there isn't a global subscription
            else:
                # Throw an error that "Subscription does not exist. Please create one instead."
                flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                return make_response(redirect(self.redirect_url))
        else:
            flash(**SnackbarMessage.WRONG_FINGERPRINT.value)
            return make_response(redirect(self.redirect_url))

    def activate_subscription(self, data):
        """Activate a subscription for the user"""
        # EXTRACT FINGERPRINT FROM THE DATA
        if self._validate_fingerprint_format(data.get("fingerprint")):
            global_sub = self._get_global_sub_for_fingerprint(data.get("fingerprint"))
            # Check if the user already has a global subscription for the fingerprint.
            if global_sub:
                # If there is a global subscription
                child_sub = getattr(global_sub, self.subscription_type.get("db_value"))  # type: ignore
                # If there is the child subscription, modify it, and commit the changes
                if child_sub:
                    child_sub.is_active = True
                    db.session.add(child_sub)
                    db.session.commit()
                    flash(**SnackbarMessage.SUBSCRIPTION_ENABLED.value)
                    return make_response(redirect(self.redirect_url))
                # If there isnt a child subscription, throw an error that sub does not exist.
                else:
                    # Throw an error that "Subscription already exists. Edit it instead!"
                    flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                    return make_response(redirect(self.redirect_url))
            # If there isn't a global subscription
            else:
                # Throw an error that "Subscription does not exist. Please create one instead."
                flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                return make_response(redirect(self.redirect_url))
        else:
            flash(**SnackbarMessage.WRONG_FINGERPRINT.value)
            return make_response(redirect(self.redirect_url))

    def delete_subscription(self, data):
        """Delete a subscription for the user"""
        # EXTRACT FINGERPRINT FROM THE DATA
        if self._validate_fingerprint_format(data.get("fingerprint")):
            global_sub = self._get_global_sub_for_fingerprint(data.get("fingerprint"))
            # Check if the user already has a global subscription for the fingerprint.
            if global_sub:
                # If there is a global subscription
                child_sub = getattr(global_sub, self.subscription_type.get("db_value"))  # type: ignore
                # If there is the child subscription, delete it!
                if child_sub:
                    db.session.delete(child_sub)
                    db.session.commit()
                    flash(**SnackbarMessage.SUBSCRIPTION_DELETED.value)
                    return make_response(redirect(self.redirect_url))
                # If there isnt a child subscription, throw an error that sub does not exist.
                else:
                    # Throw an error that "Subscription already exists. Edit it instead!"
                    flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                    return make_response(redirect(self.redirect_url))
            # If there isn't a global subscription
            else:
                # Throw an error that "Subscription does not exist. Please create one instead."
                flash(**SnackbarMessage.SUBSCRIPTION_DOES_NOT_EXIST.value)
                return make_response(redirect(self.redirect_url))
        else:
            flash(**SnackbarMessage.WRONG_FINGERPRINT.value)
            return make_response(redirect(self.redirect_url))
