from typing import Any

from tor_weather.constants import SubscriptionType
from tor_weather.core.database.tables import (
    NodeFlagExitSub,
    NodeFlagFastSub,
    NodeFlagGuardSub,
    NodeFlagStableSub,
    NodeFlagValidSub,
)
from tor_weather.dao.subscription.subscription_dao import SubscriptionDao

NODE_FLAG_MODEL = {
    "exit": NodeFlagExitSub,
    "fast": NodeFlagFastSub,
    "guard": NodeFlagGuardSub,
    "stable": NodeFlagStableSub,
    "valid": NodeFlagValidSub,
}


class NodeFlagDao(SubscriptionDao):
    def __init__(self, email: str, flag: str):
        super().__init__(email, flag)
        self.redirect_url = f"/dashboard/node-flag/{flag}"
        self.subscription_type = SubscriptionType[f"node-flag-{flag}"]

    def _create_current_sub(self, data: Any):
        """Create a current subscription object"""
        node_flag_model = NODE_FLAG_MODEL[self.flag]  # type: ignore
        subscription = node_flag_model(wait_for=data.get("wait_for"))
        return subscription

    def _modify_current_sub(self, child_sub: Any, data: Any) -> None:
        """Modify the current subscription object"""
        child_sub.wait_for = data.get("wait_for")
