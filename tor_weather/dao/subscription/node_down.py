from typing import Any

from tor_weather.constants import SubscriptionType
from tor_weather.core.database.tables import NodeDownSub
from tor_weather.dao.subscription.subscription_dao import SubscriptionDao


class NodeDownDao(SubscriptionDao):
    def __init__(self, email: str, flag=None):
        super().__init__(email)
        self.redirect_url = "/dashboard/node-status/node-down"
        self.subscription_type = SubscriptionType["node-down"]

    def _create_current_sub(self, data: Any) -> NodeDownSub:
        """Create a current subscription object"""
        subscription: NodeDownSub = NodeDownSub(wait_for=data.get("wait_for"))
        return subscription

    def _modify_current_sub(self, child_sub: NodeDownSub, data: Any) -> None:
        """Modify the current subscription object"""
        child_sub.wait_for = data.get("wait_for")
