from os import environ

# App Config

# This is the port on which the backend will run on
APP_PORT = environ.get("APP_PORT") or 5000

# App Secret

# This is the secret key for the flask application
SECRET_KEY = environ.get("SECRET_KEY")

# SMTP Secrets

# This is the host-name for the SMTP Connection
SMTP_HOST = environ.get("SMTP_HOST")
# This is the port number for the SMTP Connection
SMTP_PORT = environ.get("SMTP_PORT")
# This is the username for the SMTP Connection
SMTP_USERNAME = environ.get("SMTP_USERNAME")
# This is the password for the SMTP Connection
SMTP_PASSWORD = environ.get("SMTP_PASSWORD")
# This is the status for using SSL with SMTP
SMTP_SSL = environ.get("SMTP_SSL") or True

# Database Secrets

# This is the URI for the database connection
SQLALCHEMY_DATABASE_URI = environ.get("SQLALCHEMY_DATABASE_URI")

# API Calls

# This is the url for fetching the statuses. Ideally should be `https://onionoo.torproject.org` in production.
API_URL = environ.get("API_URL")
# This is the url for the application. In development, this shall be `http://127.0.0.1:5000`, but in production,
# it will include the domain & the subdomain.
BASE_URL = environ.get("BASE_URL")

# JWT Secret

# This is the secret for JWT's issued for user authorization on the portal.
JWT_SECRET = environ.get("JWT_SECRET")

# Email Encryption Password

# This is the encryption key used for encrypting the information in the verification link.
EMAIL_ENCRYPT_PASS = environ.get("EMAIL_ENCRYPT_PASS")

# Extra Configs

# This is required by SqlAlchemy, and should ideally be false
SQLALCHEMY_TRACK_MODIFICATIONS = environ.get("SQLALCHEMY_TRACK_MODIFICATIONS")
