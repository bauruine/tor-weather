def snake_case_to_camel_case(input_str: str) -> str:
    """Converts snaked_case input into camelCase

    Args:
        input_str (str): snake_case input value

    Returns:
        str: value converted into camelCase
    """
    temp = input_str.split("_")
    res = temp[0] + "".join(ele.title() for ele in temp[1:])
    return res
