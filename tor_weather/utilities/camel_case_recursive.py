from typing import Any

from .snake_case_to_camel_case import snake_case_to_camel_case


def camel_case_recursive(payload: dict) -> dict:
    """Converts the given object into an object with camelCased keys recursively

    Args:
        payload (dict): The object that needs to be camelCased

    Returns:
        dict: Object with camelCased Keys
    """
    new_payload: Any = {}
    for key in payload.keys():
        value: Any = payload[key]
        new_key: str = snake_case_to_camel_case(key)
        if type(value) is dict:
            new_payload[new_key] = camel_case_recursive(value)
        elif type(value) is list:
            new_payload[new_key] = [
                camel_case_recursive(list_val) if type(list_val) is dict else list_val
                for list_val in value
            ]
        else:
            new_payload[new_key] = value
    return new_payload
