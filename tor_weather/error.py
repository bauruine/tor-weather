class APIError(Exception):
    """API Error Exception

    Args:
        Exception (_type_): Python's Default Exception
    """

    def __init__(self, message: str, status_code=500):
        """Initialises the API Error Exception

        Args:
            message (str): The message to be sent with the error
            status_code (int, optional): Status code for the error. Defaults to 500.
        """
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code


class BadRequest(APIError):
    """API Error Exception for Bad Request

    Args:
        APIError (_type_): Base API Error Class
    """

    def __init__(self, message: str) -> None:
        """Initialises the Bad Request Exception

        Args:
            message (str): The message to be sent with the error
        """
        status_code = 400
        APIError.__init__(self, message, status_code)


class ResourceNotFound(APIError):
    """API Error Exception for Resource Not Found

    Args:
        APIError (_type_): Base API Error Class
    """

    def __init__(self, message: str) -> None:
        """Initialises the Resource Not Found Exception

        Args:
            message (str): The message to be sent with the error
        """
        status_code = 404
        APIError.__init__(self, message, status_code)


class UnAuthorized(APIError):
    """API Error Exception for Unauthorized Request

    Args:
        APIError (_type_): Base API Error Class
    """

    def __init__(self, message: str) -> None:
        """Initialises the Unauthorized Exception

        Args:
            message (str): The message to be sent with the error
        """
        status_code = 401
        APIError.__init__(self, message, status_code)


class InternalError(APIError):
    """API Error Exception for Internal Error

    Args:
        APIError (_type_): Base API Error Class
    """

    message = "Something went wrong."

    def __init__(self, message: str = message) -> None:
        """Initialises the Internal Error Exception

        Args:
            message (str): The message to be sent with the error
        """
        status_code = 500
        APIError.__init__(self, message, status_code)
