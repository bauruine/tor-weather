from datetime import datetime
from typing import List, Optional, TypedDict


class RelayPayloadInterface(TypedDict):
    nickname: str
    fingerprint: str
    or_addresses: List[str]
    exit_addresses: Optional[List[str]]
    dir_address: Optional[str]
    last_seen: str
    last_changed_address_or_port: str
    first_seen: str
    running: bool
    hibernating: Optional[bool]
    flags: Optional[List[str]]
    country: Optional[str]
    country_name: Optional[str]
    region_name: Optional[str]
    city_name: Optional[str]
    latitude: Optional[int]
    longitude: Optional[int]
    as_number: Optional[str]
    as_name: Optional[str]
    consensus_weight: int
    verified_host_names: Optional[List[str]]
    last_restarted: Optional[str]
    bandwidth_rate: Optional[int]
    bandwidth_burst: Optional[int]
    observed_bandwidth: Optional[int]
    advertised_bandwidth: Optional[int]
    overload_general_timestamp: Optional[int]
    exit_policy: Optional[List[str]]
    exit_policy_summary: Optional[dict]
    exit_policy_v6_summary: Optional[dict]
    contact: Optional[str]
    platform: Optional[str]
    version: Optional[str]
    recommended_version: Optional[bool]
    version_status: Optional[str]
    effective_family: Optional[List[str]]
    alleged_family: Optional[List[str]]
    indirect_family: Optional[List[str]]
    consensus_weight_fraction: Optional[int]
    guard_probability: Optional[int]
    middle_probability: Optional[int]
    exit_probability: Optional[int]
    measured: Optional[bool]
    unreachable_or_addresses: Optional[List[str]]


class BridgePayloadInterface(TypedDict):
    nickname: str
    hashed_fingerprint: str
    or_addresses: List[str]
    last_seen: str
    first_seen: str
    running: bool
    flags: Optional[List[str]]
    last_restarted: Optional[str]
    advertised_bandwidth: Optional[int]
    overload_general_timestamp: Optional[int]
    platform: Optional[str]
    version: Optional[str]
    recommended_version: Optional[bool]
    version_status: Optional[str]
    transports: Optional[List[str]]
    blocklist: Optional[List[str]]
    bridgedb_distributor: Optional[str]


class DetailsPayloadInterface(TypedDict):
    version: str
    next_major_version_scheduled: Optional[str]
    build_revision: Optional[str]
    relays_published: Optional[str]
    relays_skipped: Optional[int]
    relays: List[RelayPayloadInterface]
    relays_truncated: Optional[int]
    bridges_published: str
    bridges_skipped: Optional[int]
    bridges: List
    bridgesTruncated: Optional[int]


class VersionInterface(TypedDict):
    id: Optional[str]
    status: Optional[str]
    recommended: Optional[bool]
    platform: Optional[str]


class BandwidthInterface(TypedDict):
    observed: Optional[int]
    advertised: Optional[int]
    burst: Optional[int]
    rate: Optional[int]


class DateInterface(TypedDict):
    first_seen: datetime
    last_seen: datetime


class FlagInterface(TypedDict):
    exit: Optional[bool]
    guard: Optional[bool]
    running: Optional[bool]
    stable: Optional[bool]
    fast: Optional[bool]
    valid: Optional[bool]
