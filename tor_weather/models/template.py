from tor_weather.constants import MessageResponseTypes


class SnackbarInterface:
    messageContent: str
    messageType: MessageResponseTypes
