import enum


class CookieResponse:
    key: str
    value: str
    httpOnly: bool


class MessageResponseTypes(enum.Enum):
    SUCCESS = "success"
    WARNING = "warning"
    ERROR = "error"


class ResponseCode(enum.Enum):
    SUCCESS = "200"
    REDIRECT = "302"
    BAD_REQUEST = "400"
    UNAUTHORIZED = "401"
    NOT_FOUND = "404"
    SERVER_ERROR = "500"


class MessageResponse:
    messageType: MessageResponseTypes
    messageContent: str


class GenericResponsePayload:
    message: MessageResponse
    cookie: CookieResponse


GenericResponse = tuple[ResponseCode, GenericResponsePayload]


class RedirectResponsePayload(GenericResponsePayload):
    redirectUrl: str


RedirectResponse = tuple[ResponseCode, RedirectResponsePayload]


class SuccessResponsePayload(GenericResponsePayload):
    renderTemplate: str


SuccessResponse = tuple[ResponseCode, SuccessResponsePayload]
