from datetime import datetime

from tor_weather.extensions import db


class Subscription(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    created_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    is_family = db.Column(db.Boolean, nullable=False, default=False)
    is_extended_family = db.Column(db.Boolean, nullable=False, default=False)
    is_active = db.Column(db.Boolean, nullable=False, default=False)
    # Relationships for the Subscriber & Relay Tables
    subscriber_id = db.Column(
        db.Integer, db.ForeignKey("subscriber.id"), nullable=False
    )
    host_id = db.Column(db.Integer, db.ForeignKey("relay.id"), nullable=True)
    relay_id = db.Column(db.Integer, db.ForeignKey("relay.id"), nullable=False)
    # Relationships for the Subscription Tables
    node_down_sub = db.relationship(
        "NodeDownSub",
        foreign_keys="NodeDownSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
    node_bandwidth_sub = db.relationship(
        "NodeBandwidthSub",
        foreign_keys="NodeBandwidthSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
    node_version_sub = db.relationship(
        "NodeVersionSub",
        foreign_keys="NodeVersionSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
    # Relationships for Node Flag Subs
    node_flag_exit_sub = db.relationship(
        "NodeFlagExitSub",
        foreign_keys="NodeFlagExitSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
    node_flag_fast_sub = db.relationship(
        "NodeFlagFastSub",
        foreign_keys="NodeFlagFastSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
    node_flag_guard_sub = db.relationship(
        "NodeFlagGuardSub",
        foreign_keys="NodeFlagGuardSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
    node_flag_stable_sub = db.relationship(
        "NodeFlagStableSub",
        foreign_keys="NodeFlagStableSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
    node_flag_valid_sub = db.relationship(
        "NodeFlagValidSub",
        foreign_keys="NodeFlagValidSub.subscription_id",
        back_populates="subscription",
        uselist=False,
        lazy=True,
    )
