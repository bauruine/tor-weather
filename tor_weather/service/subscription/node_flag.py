from typing import Type

from flask import Response, url_for

from tor_weather.dao.subscription.node_flag import NodeFlagDao
from tor_weather.service.subscription_wrapper import SubscriptionService


class NodeFlagService(SubscriptionService):
    TABLE_HEADER_KEYS: list[str] = ["fingerprint", "wait_for", "is_active"]
    TABLE_DAO: Type[NodeFlagDao] = NodeFlagDao  # type: ignore

    def __init__(self, email: str, flag: str) -> None:
        super().__init__(email, flag)

    def _get_modify_url(self, id: int) -> str:
        return url_for("node_flag_modify", fingerprint=id, flag_name=self.flag)

    def _get_delete_url(self, id: str) -> str:
        return url_for("node_flag_delete_api", fingerprint=id, flag_name=self.flag)

    def _get_enable_url(self, id: str) -> str:
        return url_for("node_flag_enable_api", fingerprint=id, flag_name=self.flag)

    def _get_disable_url(self, id: str) -> str:
        return url_for("node_flag_disable_api", fingerprint=id, flag_name=self.flag)

    def get_subscriptions(self):
        return {
            "header": super()._get_table_header(),
            "content": super()._get_table_content(),
        }

    def get_subscription_data(self, fingerprint: str):
        """Get data for Node Flag Subscription for the user"""
        return self.TABLE_DAO(email=self.email, flag=self.flag).get_subscription_data(
            fingerprint
        )

    def create_subscription(self, fingerprint: str, wait_for: int) -> Response:
        """Create a Node Flag Subscription"""
        data = {"fingerprint": fingerprint, "wait_for": wait_for}
        return self.TABLE_DAO(email=self.email, flag=self.flag).create_subscription(
            data
        )

    def modify_subscription(self, fingerprint: str, wait_for: int) -> Response:
        """Modify a Node Flag Subscription"""
        data = {"fingerprint": fingerprint, "wait_for": wait_for}
        return self.TABLE_DAO(email=self.email, flag=self.flag).modify_subscription(
            data
        )

    def deactivate_subscription(self, fingerprint: str) -> Response:
        """Deactivate a Node Flag Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email, flag=self.flag).deactivate_subscription(
            data
        )

    def activate_subscription(self, fingerprint: str) -> Response:
        """Activate a Node Flag Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email, flag=self.flag).activate_subscription(
            data
        )

    def delete_subscription(self, fingerprint: str) -> Response:
        """Delete a Node Flag Subscription"""
        data = {"fingerprint": fingerprint}
        return self.TABLE_DAO(email=self.email, flag=self.flag).delete_subscription(
            data
        )
