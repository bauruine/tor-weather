info_card_data: dict[str, dict[str, str]] = {
    "fingerprint": {
        "header": "Fingerprint",
        "content": """Every relay has a unique 40 digit long upper-cased id associated with it.
        You can find the fingerprint for your relay by clicking""",
        "link": "https://metrics.torproject.org/rs.html#search",
    },
    "wait_for": {
        "header": "Waiting Time",
        "content": """Tor-Weather service will wait for this time after first noticing the anomaly
        with the relay. If the issue doesn't get resolved within this time, a notification email
        will be sent.""",
    },
    "threshold": {
        "header": "Threshold Bandwidth",
        "content": """Tor-Weather service will consider an anomaly if the average bandwidth of the
        relay falls below the threshold bandwidth specified in the subscription.""",
    },
}


class InformationCard:
    """Data for Information Card to be displayed in the template for creating & modifying subscriptions"""

    def __init__(self) -> None:
        pass

    def get_data(self, form_fields: list[str]) -> list[dict[str, str]]:
        """Get data for the information-card tiles"""
        return [info_card_data[form_field] for form_field in form_fields]
