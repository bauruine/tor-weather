import copy

# TODO: Create a model for the sidebar data
SIDEBAR_CONTENT = {
    "categories": [
        {
            "displayName": "Node Status",
            "options": [
                {
                    "displayName": "Node Down",
                    "url": "/dashboard/node-status/node-down",
                    "icon": "trending_down",
                },
                {
                    "displayName": "Node Bandwidth",
                    "url": "/dashboard/node-status/node-bandwidth",
                    "icon": "speed",
                },
            ],
        },
        {
            "displayName": "Node Flag",
            "options": [
                {
                    "displayName": "Node Exit Flag",
                    "url": "/dashboard/node-flag/exit",
                    "icon": "door_back",
                },
                {
                    "displayName": "Node Stable Flag",
                    "url": "/dashboard/node-flag/stable",
                    "icon": "done_all",
                },
                {
                    "displayName": "Node Guard Flag",
                    "url": "/dashboard/node-flag/guard",
                    "icon": "shield",
                },
                {
                    "displayName": "Node Valid Flag",
                    "url": "/dashboard/node-flag/valid",
                    "icon": "verified",
                },
                {
                    "displayName": "Node Fast Flag",
                    "url": "/dashboard/node-flag/fast",
                    "icon": "bolt",
                },
            ],
        },
        {
            "displayName": "Support",
            "options": [
                {
                    "displayName": "Get Help",
                    "url": "/get-help",
                    "icon": "info",
                },
                {
                    "displayName": "Submit Feedback",
                    "url": "/submit-feedback",
                    "icon": "chat_bubble_outline",
                },
                {"displayName": "Logout", "url": "/api/logout", "icon": "logout"},
            ],
        },
    ]
}

# TODO: Dynamically create sidebar content with data from constants maybe


class Sidebar:
    """Sidebar data to be shown for Dashboard Pages"""

    def __init__(self, route: str):
        self.route = route

    def get_data(self):
        """
        Get the list of options to be displayed in the sidebar
        """
        sidebar_content = copy.deepcopy(SIDEBAR_CONTENT)
        for subscription in sidebar_content["categories"]:
            for option in subscription["options"]:
                option["active"] = option["url"] in self.route
        return sidebar_content
