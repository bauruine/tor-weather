from tor_weather.constants import HEADER_OPTIONS


class Header:
    """
    Headers to be displayed for individual page
    """

    def __init__(self, page: str):
        self.page = page

    def get_options(self):
        """
        Get the list of options to be displayed in the header
        """
        # TODO: Assign a return type
        headers = HEADER_OPTIONS[self.page]
        return {header: True for header in headers}
