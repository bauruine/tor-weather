## Introduction

Tor-weather is a notification service which helps the relay operators get notified when some incident happens with their relays. The service is intended to have 2 major functions:

To provide notification on possible incidents with the relays.
To incentivise relay operators for the work. (PENDING)

## Usage

1. Relay operators should visit the web portal for tor-weather & register themselves.
2. Registration is minimal, requires an email-id & a password.
3. A verification-email will be sent, which will to used to validate the account.
4. Upon logging in the portal, users can subscribe to different types of notifications for relays.

## Technical Specifications

The project is built up using `Flask`. It uses the `Postgresql` as database in the production environment, and heavily relies on the `poetry` for easing out the process of setup & deployments.

## Pre-requisites

1. Python -> 3.9
2. Poetry -> Latest ([Installation Instructions](https://python-poetry.org/docs/))
3. Any database -> preferably PostgreSQL

## Dev Setup

1. Run `poetry install --with dev`
2. Create a `.env` file on the root of the project & add the required variables. (Check `src/settings.py` for the required variables)
3. Run `make dev` for running the backend.
4. Run `make job` for running the onionoo-job.

## Prod Setup

1. Run `poetry install`
2. Create a `.env` file on the root of the project & add the required variables. (Check `src/settings.py` for the required variables)
3. Make sure `FLASK_ENV` is set to `production` in environment variables.
4. Build the wheel using `poetry build --format wheel`
5. Install the wheel using `python3 -m pip install dist/tor_weather-*.whl --force-reinstall`
6. Run `python3 -m tor_weather.job` for running the job.
7. Run `python3 -m gunicorn -w 1 -b localhost:5000 tor_weather.app:app` for running the backend.

## Notes

1. Prod setup might require creating a `logs` directory in the cwd.
